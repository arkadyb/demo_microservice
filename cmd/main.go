package main

import (	
	"net/http"
	"os"
	"os/signal" 
	"syscall" 
	"fmt" 
	"bitbucket.org/arkadyb/demo_microservice/service" 
	"bitbucket.org/arkadyb/demo_microservice/data_store"	
	"bitbucket.org/arkadyb/demo_microservice/log"	
	"bitbucket.org/arkadyb/demo_microservice/endpoints"	
	kitlog "github.com/go-kit/kit/log"
	flags "github.com/jessevdk/go-flags"
	"strings"
)
 
var opts struct {
	Port *int `long:"port" description:"Port to run microservice on"`
	Host string `long:"host" description:"Host to run microservice on"`
}

func main(){
	var logger kitlog.Logger
	{
		logger = kitlog.NewLogfmtLogger(os.Stderr)
		logger = kitlog.With(logger, "ts", kitlog.DefaultTimestampUTC)
		logger = kitlog.With(logger, "caller", kitlog.DefaultCaller)
	}

	_, err := flags.ParseArgs(&opts, os.Args)
	if err!=nil{
		fmt.Println(err.Error())
		return
	}

	httpAddr:=""
	if host:=strings.TrimSpace(opts.Host); host!=""{
		httpAddr=host
	}

	port:=8080
	if opts.Port!=nil{
		port = *opts.Port 
	}
	httpAddr+=fmt.Sprintf(":%d", port)


	var s service.Interface
	{
		ds, err := data_store.NewFileStore("durations.json")
		if err!=nil{ 
			logger.Log("Failed create file store", err.Error())
			return
		}

		s, err = service.NewDemoService(ds)
		if err!=nil{
			logger.Log("Failed to run new service", err.Error())
			return
		}
		s = log.LoggingMiddleware(logger)(s)
	}

	var h http.Handler
	{
		h, err = endpoints.MakeHTTPHandlers(s, kitlog.With(logger, "component", "HTTP"))
		if err!=nil{
			logger.Log("Failed to setup handlers", err.Error())
			return
		}
	}

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		logger.Log("transport", "HTTP", "addr", httpAddr)
		errs <- http.ListenAndServe(httpAddr, h)
	}()

	logger.Log("exit", <-errs)
}