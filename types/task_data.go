package types

type Task struct{
	ID *int `json:"id"`
	Duration *int `json:"duration"`
}