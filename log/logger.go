package log

import (
	"bitbucket.org/arkadyb/demo_microservice/service"
	"github.com/go-kit/kit/log"
	"context"
	"bitbucket.org/arkadyb/demo_microservice/types"
	"time"
	"encoding/json"
)
/*
	Implementation of go-kit logging middleware for logging of demo service routines payloads
 */

type Middleware func(service.Interface) service.Interface

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next service.Interface) service.Interface {
		return &loggerMiddlerware{
			next:   next,
			logger: logger,
		}
	}
}

type loggerMiddlerware struct {
	next   service.Interface
	logger log.Logger
}

func (mw *loggerMiddlerware) GetAverage(ctx context.Context, id int) (average float64, err error){
	defer func(begin time.Time) {
		mw.logger.Log("method", "GetAverage", "id", id, "took", time.Since(begin), "average", average, "err", err)
	}(time.Now())

	return mw.next.GetAverage(ctx, id)
}

func (mw *loggerMiddlerware) TaskPerformed(ctx context.Context, task types.Task) (success bool, err error){
	defer func(begin time.Time) {
		jTask := ""
		if data, merr := json.Marshal(task); merr == nil{
			jTask = string(data)
			mw.logger.Log("method", "TaskPerformed", "data", jTask, "took", time.Since(begin), "success", success, "err", err)
		}else{
			mw.logger.Log("method", "TaskPerformed", "data", "", "took", time.Since(begin), "success", success, "err", err, "marshalingerror", merr, )
		}
	}(time.Now())

	return mw.next.TaskPerformed(ctx, task)
}
