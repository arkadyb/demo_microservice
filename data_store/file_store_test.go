package data_store

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"demo_microservice/types"
	"encoding/json"
	"io/ioutil"
	"os"
)

func TestNewFileStore(t *testing.T) {
	t.Run("Success", func(t *testing.T){
		store, err:=NewFileStore("test_data/test_store.json")
		assert.NoError(t, err)
		assert.NotNil(t, store)
	})

	t.Run("Empty name", func(t *testing.T){
		store, err:=NewFileStore("")
		assert.Error(t, err)
		assert.Nil(t, store)
	})
}

func TestFileStore_Get(t *testing.T) {
	t.Run("Success", func(t *testing.T){
		store, err:=NewFileStore("test_data/durations.json")
		assert.NoError(t, err)

		if assert.NotNil(t, store){
			tasks, err := store.Get(1)
			assert.NoError(t, err)
			assert.Len(t, tasks, 3)

			for _, task:= range tasks{
				assert.NotNil(t, task)
			}
		}
	})

	t.Run("No data", func(t *testing.T){
		store, err:=NewFileStore("test_data/durations.json")
		assert.NoError(t, err)

		if assert.NotNil(t, store){
			tasks, err := store.Get(2)
			assert.NoError(t, err)
			assert.Len(t, tasks, 0)
		}
	})

	t.Run("Bad json", func(t *testing.T){
		store, err:=NewFileStore("test_data/bad_format.json")
		assert.NoError(t, err)

		if assert.NotNil(t, store){
			tasks, err := store.Get(1)
			assert.Error(t, err)
			assert.Nil(t, tasks)
		}
	})
}

func TestFileStore_Add(t *testing.T) {
	id:=1
	duration:=100

	t.Run("Success", func(t *testing.T){
		filename:="test_data/temp.json"
		store, err:=NewFileStore(filename)
		assert.NoError(t, err)
		assert.NotNil(t, store)

		err = store.Add(types.Task{ID:&id, Duration:&duration})
		assert.NoError(t, err)

		tasksById :=make(map[int][]types.Task)
		bts, err := ioutil.ReadFile(filename)
		err = json.Unmarshal(bts, &tasksById)
		if assert.NoError(t, err){
			tasks := tasksById[1]
			assert.NotNil(t, tasks)
			assert.Len(t, tasks, 1)
		}
		os.Remove(filename)
	})

	t.Run("Bad json", func(t *testing.T){
		store, err:=NewFileStore("test_data/bad_format.json")
		assert.NoError(t, err)
		assert.NotNil(t, store)

		err = store.Add(types.Task{ID:&id, Duration:&duration})
		assert.Error(t, err)
	})
}