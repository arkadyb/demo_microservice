package data_store

import (
	"io/ioutil"
	"strings" 
	"os"
	"errors"
	"encoding/json"
	"bitbucket.org/arkadyb/demo_microservice/types"
	"sync"
	"bytes"
)

//NewFileStore creates a new customers json store
func NewFileStore(filename string) (*FileStore, error){
	if strings.TrimSpace(filename)==""{
		return nil, errors.New("filename cant be empty")
	}

	if _, err := os.Stat(filename); err != nil {
		if err:=ioutil.WriteFile(filename, bytes.NewBufferString("{}").Bytes(), 0777); err!=nil{
			return nil, err
		}
	}

	fs:=new(FileStore)
	fs.filename = strings.TrimSpace(filename)

	return fs, nil
}

//FileStore implements data store Interface for customers list saved in json file_store
type FileStore struct{
	mtx sync.RWMutex
	filename string
}

//GetCustomers returns a list of customers from given json file_store; or error
func (fs *FileStore) Add(task types.Task) error{
	fs.mtx.Lock()
	defer fs.mtx.Unlock()

	if _, err := os.Stat(fs.filename); err != nil {
		return err
	}

	tasksById :=make(map[int][]types.Task)

	bts, err := ioutil.ReadFile(fs.filename)
	if err != nil {
		return err
	}

	err = json.Unmarshal(bts, &tasksById)
	if err != nil {
		return err
	}

	var tasks []types.Task
	ok:=false
	if tasks, ok = tasksById[*task.ID];ok{
	}

	tasks = append(tasks, task)
	tasksById[*task.ID] = tasks

	bts, err = json.Marshal(tasksById)
	if err!=nil{
		return err
	}

	err = ioutil.WriteFile(fs.filename, bts, 0777)
	if err!=nil{
		return err
	}

	return nil
}

func (fs *FileStore) Get(id int) ([]types.Task, error) {
	fs.mtx.RLock()
	defer fs.mtx.RUnlock()

	if _, err := os.Stat(fs.filename); err != nil {
		return nil, err
	}

	bts, err := ioutil.ReadFile(fs.filename)
	if err != nil {
		return nil, err
	}

	tasksById :=make(map[int][]types.Task)
	err = json.Unmarshal(bts, &tasksById)
	if err != nil {
		return nil, err
	}

	if tasks, ok := tasksById[id]; ok {

		return tasks, nil
	}

	return []types.Task{}, nil
}
