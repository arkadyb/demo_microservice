package data_store

import "bitbucket.org/arkadyb/demo_microservice/types"

//DataStore interface required to be implemented by any data store to be used in microservice
type Interface interface{
	Add(data types.Task) error
	Get(id int) ([]types.Task, error)
}