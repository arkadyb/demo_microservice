package service

import (
	"bitbucket.org/arkadyb/demo_microservice/data_store"	
	"bitbucket.org/arkadyb/demo_microservice/types"
	"context"
	"errors"
)

//NewDemoService created a new demoService instance
func NewDemoService(dataStore data_store.Interface) (Interface, error){
	if dataStore==nil{
		return nil, errors.New("Data store cant bi nil")
	}

	srv:=new(demoService)
	srv.store = dataStore

	return srv, nil
}

//demoService is a demo implementation of Service.Interface
type demoService struct{
	store data_store.Interface
}

//Service.Interface implementation
func (ds *demoService) GetAverage(ctx context.Context, id int) (float64, error){
	var average float64

	tasks, err :=ds.store.Get(id)
	if err!=nil{
		return 0, err
	}

	if tasks!=nil && len(tasks) > 0 {
		for _, duration := range tasks {
			average+=float64(*duration.Duration)
		}
		average /=float64(len(tasks))
	}

	return average, nil
}

//Service.Interface implementation
func (ds *demoService) TaskPerformed(ctx context.Context, task types.Task) (bool, error){

	err := ds.store.Add(task)

	if err!=nil {

		return false, err
	}

	return true, nil
}
