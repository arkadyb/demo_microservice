package service

import ( 
	"context"
	"bitbucket.org/arkadyb/demo_microservice/types"
)

//Interface describes demo service main routines
type Interface interface {
	//TaskPerformed adds task to the store
	TaskPerformed(ctx context.Context, t types.Task) (bool, error)

	//GetAverage returns average time spent to perform all tasks with given id
	GetAverage(ctx context.Context, id int) (float64, error)
}