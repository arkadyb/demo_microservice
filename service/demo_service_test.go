package service

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"bitbucket.org/arkadyb/demo_microservice/types"
	"context"
	"errors"
)

type MockedStore struct {
	mock.Mock

}

func (ms *MockedStore) Add(task types.Task) error{
	args := ms.Called(task)
	return args.Error(0)
}

func (ms *MockedStore) Get(id int) ([]types.Task, error) {
	args := ms.Called(id)
	if args.Get(0)!=nil{
		return args.Get(0).([]types.Task), args.Error(1)
	}

	return nil, args.Error(1)
}



func TestNewDemoService(t *testing.T) {

	t.Run("Nil store; Error expected", func(t *testing.T){
		ds, err:=NewDemoService(nil)

		assert.Error(t, err)
		assert.Nil(t, ds)
	})

	t.Run("Success", func(t *testing.T){
		store:=new(MockedStore)
		ds, err:=NewDemoService(store)

		assert.NoError(t, err)
		assert.NotNil(t, ds)
	})
}

func TestDemoService_GetAverage(t *testing.T) {
	ds:=new(demoService)

	t.Run("Success", func(t *testing.T){
		store:=new(MockedStore)

		id:=1
		duration:=100
		store.On("Get", 1).Return(
			[]types.Task{
				{ID: &id, Duration: &duration},
				{ID: &id, Duration: &duration},
				{ID: &id, Duration: &duration},
				{ID: &id, Duration: &duration},
			}, nil,
		)
		ds.store = store

		res, err := ds.GetAverage(context.Background(), 1)
		if assert.NoError(t, err) {
			assert.Equal(t, float64(100), res)
		}
	})

	t.Run("No tasks stored", func(t *testing.T){
		store:=new(MockedStore)
		store.On("Get", 1).Return(
			[]types.Task{}, nil,
		)
		ds.store = store
		res, err := ds.GetAverage(context.Background(), 1)
		if assert.NoError(t, err){
			assert.Equal(t, float64(0), res)
		}
	})

	t.Run("Nil result from the storage", func(t *testing.T){
		store:=new(MockedStore)
		store.On("Get", 1).Return(
			nil, nil,
		)
		ds.store = store

		res, err := ds.GetAverage(context.Background(), 1)
		if assert.NoError(t, err){
			assert.Equal(t, float64(0), res)
		}
	})

	t.Run("Failed to get from storage", func(t *testing.T){
		store:=new(MockedStore)
		store.On("Get", 1).Return(
			nil, errors.New("Storage error"),
		)
		ds.store = store

		res, err := ds.GetAverage(context.Background(), 1)
		assert.Error(t, err)
		assert.Equal(t, float64(res), err)
	})
}

func TestDemoService_TaskPerformed(t *testing.T) {
	ds:=new(demoService)

	id:=1
	duration:=100
	task:=types.Task{
		ID: &id,
		Duration: &duration,
	}

	t.Run("Success", func(t *testing.T){
		store:=new(MockedStore)
		store.On("Add", task).Return(nil)
		ds.store = store

		success,err:=ds.TaskPerformed(context.Background(), task)
		assert.NoError(t, err)
		assert.True(t, success)
	})

	t.Run("Storage failure", func(t *testing.T){
		store:=new(MockedStore)
		store.On("Add", task).Return(errors.New("Storage error"))
		ds.store = store

		success, err:=ds.TaskPerformed(context.Background(), task)
		assert.Error(t, err)
		assert.False(t, success)
	})
}