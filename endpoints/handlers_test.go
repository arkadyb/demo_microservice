package endpoints

import (
	"testing"
	"github.com/stretchr/testify/mock"
	"context"
	"bitbucket.org/arkadyb/demo_microservice/types"
	"github.com/stretchr/testify/assert"
)

type MockedLogger struct{
	mock.Mock
}

func (ml *MockedLogger) Log(keyvals ...interface{}) error{
	args := ml.Called(keyvals)
	return args.Error(0)
}

type MockedService struct{
	mock.Mock
}

func (ms *MockedService) TaskPerformed(ctx context.Context, t types.Task) (bool, error){
	args := ms.Called(ctx ,t)
	return args.Get(0).(bool), args.Error(1)
}

func (ms *MockedService)GetAverage(ctx context.Context, id int) (float64, error){
	args := ms.Called(ctx, id)
	return args.Get(0).(float64), args.Error(1)
}

func TestMakeHTTPHandler(t *testing.T) {

	t.Run("Success", func(t *testing.T){

		srv:=new(MockedService)
		logger:=new(MockedLogger)
		handler,err:= MakeHTTPHandlers(srv, logger)

		assert.NoError(t, err)
		assert.NotNil(t, handler)

	})

	t.Run("Nil values", func(t *testing.T){
		srv:=new(MockedService)
		logger:=new(MockedLogger)

		handler, err:= MakeHTTPHandlers(srv, nil)
		assert.Error(t, err)
		assert.Nil(t, handler)

		handler, err= MakeHTTPHandlers(nil, logger)
		assert.Error(t, err)
		assert.Nil(t, handler)

	})
}