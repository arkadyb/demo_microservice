package endpoints

import (
	"net/http"
	"github.com/gorilla/mux"
	"bitbucket.org/arkadyb/demo_microservice/endpoints/average"

	"bitbucket.org/arkadyb/demo_microservice/service"
	"bitbucket.org/arkadyb/demo_microservice/endpoints/tasks"
	"github.com/go-kit/kit/log"
	"errors"
)

//MakeHTTPHandlers used to setup service routing and assign respective handling functions
func MakeHTTPHandlers(s service.Interface, logger log.Logger) (http.Handler, error) {

	if s == nil{
		return nil, errors.New("Interface instance cant be nil")
	}

	if logger == nil{
		return nil, errors.New("Logger instance cant be nil")
	}

	r := mux.NewRouter().StrictSlash(true)
	e, _ := MakeServerEndpoints(s)

	// POST    /tasks/			adds task information into the storage
	r.Methods("POST").Path("/tasks/").Handler(tasks.Handler(e.PostTaskPerformedEndpoint, logger))

	// GET     /tasks/:id/time	retrieves a given task`s average duration
	r.Methods("GET").Path("/tasks/{id}/time").Handler(average.Handler(e.GetAverageEndpoint, logger))

	return r, nil
}
