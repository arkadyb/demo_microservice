package endpoints

import (
	"github.com/go-kit/kit/endpoint"
	"bitbucket.org/arkadyb/demo_microservice/service"	
	"bitbucket.org/arkadyb/demo_microservice/endpoints/average"	
	"bitbucket.org/arkadyb/demo_microservice/endpoints/tasks"	
	"errors"
)

//MakeServerEndpoints assigns requests handling to respective service end points
func MakeServerEndpoints(s service.Interface) (*Endpoints, error) {
	if s==nil{
		return nil, errors.New("Service instance cant be nil")
	}

	return &Endpoints{
		PostTaskPerformedEndpoint: tasks.CreateEndpoint(s),
		GetAverageEndpoint:        average.CreateEndpoint(s),
	}, nil
}

type Endpoints struct {
	PostTaskPerformedEndpoint endpoint.Endpoint
	GetAverageEndpoint        endpoint.Endpoint
}


