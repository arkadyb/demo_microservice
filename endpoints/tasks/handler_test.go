package tasks

import (
	"testing"
	"github.com/stretchr/testify/mock"
	"context"
	"demo_microservice/types"
	"github.com/stretchr/testify/assert"
)

type MockedService struct{
	mock.Mock
}

func (ms *MockedService) TaskPerformed(ctx context.Context, t types.Task) (bool, error){
	args := ms.Called(ctx ,t)
	return args.Get(0).(bool), args.Error(1)
}

func (ms *MockedService) GetAverage(ctx context.Context, id int) (float64, error){
	args := ms.Called(ctx, id)
	return args.Get(0).(float64), args.Error(1)
}

func TestCreateEndpoint(t *testing.T) {
	srv:=new(MockedService)
	id:=1
	duration:=100

	t.Run("Success", func(t *testing.T){
		srv.On(
			"TaskPerformed",
			context.Background(),
			types.Task{ID: &id, Duration: &duration}).Return(
			true, nil,
		)

		endpoint:=CreateEndpoint(srv)
		assert.NotNil(t, endpoint)
	})

	t.Run("Failures", func(t *testing.T){
		endpoint:=CreateEndpoint(srv)

		_, err := endpoint(context.Background(), 1)
		assert.Error(t, err)

		_, err =endpoint(context.Background(), postTasksRequest{})
		assert.Error(t, err)

		_, err =endpoint(context.Background(), postTasksRequest{Task:&types.Task{}})
		assert.Error(t, err)

		_, err =endpoint(context.Background(), postTasksRequest{Task:&types.Task{ID: &id}})
		assert.Error(t, err)
	})
}