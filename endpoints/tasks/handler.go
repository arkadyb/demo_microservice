package tasks

import (
	kithttp "github.com/go-kit/kit/transport/http"
	"bitbucket.org/arkadyb/demo_microservice/service"
	"github.com/go-kit/kit/endpoint"
	"context"
	"net/http"
	"github.com/go-kit/kit/log"
	"errors"
	kiterrors "bitbucket.org/arkadyb/demo_microservice/errors"
)

//CreateEndpoint returns /tasks/ server Endpoint object
func CreateEndpoint(s service.Interface) endpoint.Endpoint{
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		var req postTasksRequest

		var ok bool
		if req, ok = request.(postTasksRequest);!ok{
			return nil, errors.New("Failed to convert request to postTasksRequest")
		}

		if req.Task == nil{
			return nil, errors.New("No task found")
		}

		if req.Task.ID == nil{
			return nil, errors.New("ID required")
		}

		if req.Task.Duration == nil{
			return nil, errors.New("Duration required")
		}

		success, err := s.TaskPerformed(ctx, *req.Task)
		return postTasksResponse{Success: success, Err: err}, nil
	}
}

//Tasks server handler
func Handler(e endpoint.Endpoint, logger log.Logger) http.Handler{
	return kithttp.NewServer(
		e,
		decodeTaskRequest,
		encodeResponse,
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(kiterrors.EncodeError),
	)
}



