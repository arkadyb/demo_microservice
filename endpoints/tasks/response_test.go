package tasks

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"demo_microservice/errors"
)

func TestPostTasksResponse_Error(t *testing.T) {
	resp:=postTasksResponse{}
	assert.Implements(t, (*errors.Errorer)(nil), resp)
}