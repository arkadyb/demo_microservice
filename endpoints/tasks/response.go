package tasks

import (
	"context"
	"net/http"
	kiterrors "bitbucket.org/arkadyb/demo_microservice/errors"
	"encoding/json"
)

//postTasksResponse type is used as payload for /tasks/ response
type postTasksResponse struct {
	Success bool `json:"success"`
	Err error `json:"err,omitempty"`
}

//Error implements Errorer interface
func (r postTasksResponse) Error() error { return r.Err }

//encodeResponse used to handle business logic erros for response types implementing Errorer interface
func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(kiterrors.Errorer); ok && e.Error() != nil {
		kiterrors.EncodeError(ctx, e.Error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}