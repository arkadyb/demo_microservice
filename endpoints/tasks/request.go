package tasks

import (
	"bitbucket.org/arkadyb/demo_microservice/types"
	"context"
	"net/http"
	"encoding/json"
)

//postTasksRequest type used to decode /tasks/ request payload  
type postTasksRequest struct {
	Task *types.Task
}

//decodeTaskRequest decodes /tasks/ request payload
func decodeTaskRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req postTasksRequest
	if e := json.NewDecoder(r.Body).Decode(&req.Task); e != nil {
		return nil, e
	}

	return req, nil
}