package endpoints

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestMakeServerEndpoints(t *testing.T) {

	t.Run("Success", func(t *testing.T){

		srv:=new(MockedService)
		endpoints, err:=MakeServerEndpoints(srv)
		assert.NoError(t, err)
		if assert.NotNil(t, endpoints){
			assert.NotNil(t, endpoints.GetAverageEndpoint)
			assert.NotNil(t, endpoints.PostTaskPerformedEndpoint)
		}
	})

	t.Run("Nil service", func (t *testing.T){
		endpoints, err:=MakeServerEndpoints(nil)
		assert.Error(t, err)
		assert.Nil(t, endpoints)
	})
}