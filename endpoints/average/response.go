package average

import (
	"context"
	"net/http"
	kiterrors "bitbucket.org/arkadyb/demo_microservice/errors"
	"encoding/json"
)

//getAverageResponse type is used as payload for /tasks/:id/time response
type getAverageResponse struct {
	Average float64 `json:"average"`
	Err     error   `json:"err,omitempty"`
}

//Error implements Errorer interface
func (r getAverageResponse) Error() error { return r.Err }

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(kiterrors.Errorer); ok && e.Error() != nil {
		kiterrors.EncodeError(ctx, e.Error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}