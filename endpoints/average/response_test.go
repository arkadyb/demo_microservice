package average

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"demo_microservice/errors"
)

func TestGetAverageResponse_Error(t *testing.T) {
	resp:=getAverageResponse{}
	assert.Implements(t, (*errors.Errorer)(nil), resp)
}