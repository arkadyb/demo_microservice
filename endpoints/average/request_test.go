package average

import (
	"testing"
	"net/http/httptest"
	"context"
	"github.com/stretchr/testify/assert"
)

func TestDecodeGetAverageRequest(t *testing.T){
	t.Run("No id", func(t *testing.T){
		req:=httptest.NewRequest("GET", "http://localhost", nil)
		_, err:=decodeGetAverageRequest(context.Background(), req)
		assert.Error(t, err)
	})
}