package average

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"context"
	"demo_microservice/types"
)

type MockedService struct{
	mock.Mock
}

func (ms *MockedService) TaskPerformed(ctx context.Context, t types.Task) (bool, error){
	args := ms.Called(ctx ,t)
	return args.Get(0).(bool), args.Error(1)
}

func (ms *MockedService) GetAverage(ctx context.Context, id int) (float64, error){
	args := ms.Called(ctx, id)
	return args.Get(0).(float64), args.Error(1)
}

func TestCreateEndpoint(t *testing.T) {
	srv:=new(MockedService)

	t.Run("Success", func(t *testing.T){
		srv.On(
			"GetAverage",
			context.Background(), 1).Return(
			getAverageResponse{Average: 100, Err: nil}, nil,
		)

		endpoint:=CreateEndpoint(srv)
		assert.NotNil(t, endpoint)
	})
}