package average

import (
	"github.com/gorilla/mux"
	"strconv"
	"context"
	"net/http"
	"errors"
)

//getAverageRequest type used to decode /tasks/:id/time request payload
type getAverageRequest struct{
	ID int
}

//decodeGetAverageRequest decodes /tasks/:id/time request payload
func decodeGetAverageRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	sId, ok := vars["id"]
	if !ok {
		return nil, errors.New("Id missing")
	}

	if id, err := strconv.Atoi(sId); err == nil {
		return  getAverageRequest{ID: id}, nil
	}

	return nil, errors.New("Bad id format")
}