package average

import (
	kithttp "github.com/go-kit/kit/transport/http"
	"net/http"
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"bitbucket.org/arkadyb/demo_microservice/service"	
	kiterrors "bitbucket.org/arkadyb/demo_microservice/errors"
)
 
//CreateEndpoint returns /tasks/:id/time server Endpoint object
func CreateEndpoint(s service.Interface) endpoint.Endpoint{
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getAverageRequest)
		a, e := s.GetAverage(ctx, req.ID)
		return getAverageResponse{Average: a, Err: e}, nil
	}
}

//AverageTime server handler
func Handler(e endpoint.Endpoint, logger log.Logger) http.Handler{
	return kithttp.NewServer(
		e,
		decodeGetAverageRequest,
		encodeResponse,
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(kiterrors.EncodeError),
	)
}