package errors

import (
	"context"
	"net/http"
	"encoding/json"
)

//Errorer is an interface to be used on response object in order to return a respective business logic error
type Errorer interface {
	Error() error
}

//EncodeError writes error data into ResponseWriter object
func EncodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}