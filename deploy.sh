#!/usr/bin/env bash

CGO_ENABLED=0 go build -o demoservice cmd/main.go
docker build . -t arkady/demo:latest