FROM alpine

ADD demoservice /

ENV APP_ROOT=/
RUN apk update

EXPOSE 80
CMD ["/demoservice"]